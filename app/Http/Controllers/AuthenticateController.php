<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use App\User; 
use Sentinel,Validator; 
use Illuminate\Support\Facades\Input; 
class AuthenticateController extends Controller
{

   

   public function index()
   {
       // Retrieve all the users in the database and return them
       $users = Sentinel::getUserRepository()->get();
       return $users;
   }
   

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function signup($value='')
    {
        $data =Input::all();

        $role =Sentinel::findRoleByName('client');
        $password = $data[ 'password'];

        $rules = array(
            'first_name'    => 'required|min:1', 
            'last_name'    => 'required|min:1',
            'password'    => 'required|min:1',
            
        );

        $validator = Validator::make($data, $rules);
        
        if ($validator->fails())
        {
            return response()->json(['Email and password required.'], 500);
        }

         $credentials = [
                'email' => $data['email'],
                 
        ];

        // return (Sentinel::findByCredentials($credentials)); 

         if ( Sentinel::findByCredentials($credentials) ) 
         {
             
            
            $error = 'User already exists with this email.';
           return response()->json([ $error ], 500);
             
         }

         $user =Sentinel::create([
             'first_name' => $data['first_name'],
             'last_name' => $data['last_name'],
             'email' => $data['email'],
             
             'password' => $data['password'],
             'permissions' => [
                 'user.delete' => 0,
             ],
        ]);

         // attach the user to the role
        $role->users()->attach($user);

        // create a new activation for the registered user
        $activation = (new \Cartalyst\Sentinel\Activations\IlluminateActivationRepository)->create($user);

        
        $url = url('/'); 

        $activation_link = "{$url}/user/activate?code={$activation->code}&login={$user->id}";

        $success_message = "Please check your email to complete your account registration."; 

        return response()->json( $success_message );

    }


}